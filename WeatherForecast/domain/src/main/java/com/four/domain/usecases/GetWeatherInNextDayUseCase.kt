package com.four.domain.usecases

import com.four.domain.common.BaseFlowableUseCase
import com.four.domain.common.FlowableRxTransformer
import com.four.domain.entities.openweather.WeatherNextDayEntity
import com.four.domain.repositories.WeatherRepository
import io.reactivex.Flowable

class GetWeatherInNextDayUseCase(
    transformer: FlowableRxTransformer<WeatherNextDayEntity>,
    private val repository: WeatherRepository
) : BaseFlowableUseCase<WeatherNextDayEntity, GetWeatherInNextDayUseCase.Params>(transformer) {
    override fun createFlowable(
        data: Map<String, Any>?,
        params: Params?
    ): Flowable<WeatherNextDayEntity> {
        return repository.getWeatherInNextDay(params!!.idCity)
    }

    fun getWeatherNextDay(params: Params): Flowable<WeatherNextDayEntity> {
        return single(params)
    }

    class Params(
        val idCity: String
    )
}