package com.four.domain.repositories

import com.four.domain.entities.openweather.HitsItemEntity
import com.four.domain.entities.openweather.SearchResponseEntity
import io.reactivex.Flowable

interface SearchRepository {
    // Remote
    fun getCityWithQuery(query: String) : Flowable<SearchResponseEntity>

}