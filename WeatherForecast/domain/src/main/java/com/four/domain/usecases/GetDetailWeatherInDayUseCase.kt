package com.four.domain.usecases

import com.four.domain.common.BaseFlowableUseCase
import com.four.domain.common.FlowableRxTransformer
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.repositories.WeatherRepository
import io.reactivex.Flowable

class GetDetailWeatherInDayUseCase(
    transformer: FlowableRxTransformer<WeatherCurrentDayEntity>,
    private val repository: WeatherRepository
) : BaseFlowableUseCase<WeatherCurrentDayEntity, GetDetailWeatherInDayUseCase.Params>(transformer) {

    fun getDetailDay(params: Params): Flowable<WeatherCurrentDayEntity> {
        return single(params)
    }

    class Params(
        val idCity: String
    )

    override fun createFlowable(data: Map<String, Any>?, params: Params?): Flowable<WeatherCurrentDayEntity> {
        return repository.getDetailWeatherInDay(params!!.idCity)
    }
}