package com.four.domain.entities.openweather

data class HitsItemEntity(
    val id: Int,
    val name: String,
    val country: String,
    val coord: CoordEntity
)