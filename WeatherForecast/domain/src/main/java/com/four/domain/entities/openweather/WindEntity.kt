package com.four.domain.entities.openweather

data class WindEntity (
    var id: Int = 0,
    var speed:String? = null
)