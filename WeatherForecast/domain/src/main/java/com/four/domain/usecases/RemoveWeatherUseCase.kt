package com.four.domain.usecases

import com.four.domain.repositories.WeatherRepository

class RemoveWeatherUseCase(private val repository: WeatherRepository) {
    fun removeCity(id: String) {
        repository.removeWeatherOfCity(id)
    }
}