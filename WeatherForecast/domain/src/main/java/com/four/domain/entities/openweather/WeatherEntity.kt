package com.four.domain.entities.openweather

data class WeatherEntity(
    var id: Int,
    var icon: String? = null,
    var main: String? = null
)