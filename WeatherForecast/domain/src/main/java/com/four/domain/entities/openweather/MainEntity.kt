package com.four.domain.entities.openweather

data class MainEntity(
    var id: Int = 0,
    var temp:String? = null,
    var humidity:String? = null,
    var pressure:String?=null,
    var temp_min:String?= null,
    var temp_max:String?=null
)