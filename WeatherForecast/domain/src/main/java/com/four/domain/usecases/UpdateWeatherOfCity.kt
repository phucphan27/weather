package com.four.domain.usecases

import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.repositories.WeatherRepository

class UpdateWeatherOfCity(private val repository: WeatherRepository) {
    fun updateWeatherOfCity(entity: WeatherCurrentDayEntity) {
        repository.updateWeatherOfCity(entity)
    }
}