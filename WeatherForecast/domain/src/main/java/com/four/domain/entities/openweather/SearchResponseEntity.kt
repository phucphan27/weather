package com.four.domain.entities.openweather

data class SearchResponseEntity(
    val hits: List<HitsItemEntity>
)