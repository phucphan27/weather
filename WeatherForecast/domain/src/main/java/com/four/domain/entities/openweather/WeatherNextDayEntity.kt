package com.four.domain.entities.openweather

data class WeatherNextDayEntity(
    var cod: String,
    var list: List<ItemWeather> = emptyList()
)

data class ItemWeather(
    var dt: Long?,
    var dtTxt: String? = null,
    var main: MainEntity,
    var weather: List<WeatherEntity>
)