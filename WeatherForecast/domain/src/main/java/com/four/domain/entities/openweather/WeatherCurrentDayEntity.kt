package com.four.domain.entities.openweather

data class WeatherCurrentDayEntity(
    var main: MainEntity,
    var coord: CoordEntity,
    var weather: List<WeatherEntity>,
    var forecast: List<ItemWeather>,
    var wind:WindEntity,
    var name: String,
    var date:String,
    var id:String
)