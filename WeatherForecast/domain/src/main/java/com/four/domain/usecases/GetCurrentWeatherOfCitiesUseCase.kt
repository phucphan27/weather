package com.four.domain.usecases

import com.four.domain.common.BaseFlowableUseCase
import com.four.domain.common.FlowableRxTransformer
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.repositories.WeatherRepository
import io.reactivex.Flowable

class GetCurrentWeatherOfCitiesUseCase(
    transformer: FlowableRxTransformer<MutableList<WeatherCurrentDayEntity>>,
    private val repository: WeatherRepository
) : BaseFlowableUseCase<MutableList<WeatherCurrentDayEntity>, Any>(transformer) {

    override fun createFlowable(data: Map<String, Any>?, params: Any?): Flowable<MutableList<WeatherCurrentDayEntity>> {
        return repository.getCacheWeather()
    }

    fun getWeather(): Flowable<MutableList<WeatherCurrentDayEntity>> {
        return single()
    }
}