package com.four.domain.repositories

import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.entities.openweather.WeatherNextDayEntity
import io.reactivex.Flowable

interface WeatherRepository {
    fun getApiWeather(name: String): Flowable<WeatherCurrentDayEntity>
    fun getCacheWeather(): Flowable<MutableList<WeatherCurrentDayEntity>>
    fun getWeatherInNextDay(id: String): Flowable<WeatherNextDayEntity>
    fun getDetailWeatherInDay(id: String): Flowable<WeatherCurrentDayEntity>
    fun removeWeatherOfCity(id: String)
    fun updateWeatherOfCity(entity: WeatherCurrentDayEntity)
}