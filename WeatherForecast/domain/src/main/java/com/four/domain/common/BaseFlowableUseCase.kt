package com.four.domain.common

import io.reactivex.Flowable

abstract class BaseFlowableUseCase<T, P>(
    private val transformer: FlowableRxTransformer<T>
) {

    abstract fun createFlowable(data: Map<String, Any>? = null, params: P?): Flowable<T>

    fun single(withData: Map<String, Any>? = null, params: P): Flowable<T> {
        return createFlowable(withData, params).compose(transformer)
    }

    fun single(): Flowable<T> {
        return createFlowable(null, null).compose(transformer)
    }

    fun single(params: P): Flowable<T> {
        return createFlowable(null, params).compose(transformer)
    }

}