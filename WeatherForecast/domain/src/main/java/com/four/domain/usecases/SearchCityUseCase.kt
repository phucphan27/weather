package com.four.domain.usecases

import com.four.domain.common.BaseFlowableUseCase
import com.four.domain.common.FlowableRxTransformer
import com.four.domain.entities.openweather.SearchResponseEntity
import com.four.domain.repositories.SearchRepository
import io.reactivex.Flowable

class SearchCityUseCase (
    transformer: FlowableRxTransformer<SearchResponseEntity>,
    private val repository: SearchRepository
): BaseFlowableUseCase<SearchResponseEntity, SearchCityUseCase.SearchCityParams>(transformer){

    class SearchCityParams(
        var query: String
    )

    override fun createFlowable(
        data: Map<String, Any>?,
        params: SearchCityParams?
    ): Flowable<SearchResponseEntity> {
        return repository.getCityWithQuery(params!!.query)
    }

    fun getCityWithQuery(params: SearchCityParams): Flowable<SearchResponseEntity>{
        return single(params)
    }

}