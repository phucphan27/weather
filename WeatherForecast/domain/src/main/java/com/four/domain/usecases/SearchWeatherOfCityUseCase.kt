package com.four.domain.usecases

import com.four.domain.common.BaseFlowableUseCase
import com.four.domain.common.FlowableRxTransformer
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.repositories.WeatherRepository
import io.reactivex.Flowable

class SearchWeatherOfCityUseCase(
    transformer: FlowableRxTransformer<WeatherCurrentDayEntity>,
    private val repository: WeatherRepository
) : BaseFlowableUseCase<WeatherCurrentDayEntity, SearchWeatherOfCityUseCase.Params>(transformer) {
    override fun createFlowable(
        data: Map<String, Any>?,
        params: Params?
    ): Flowable<WeatherCurrentDayEntity> {
        return repository.getApiWeather(params!!.name)
    }

    fun getWeather(params: Params): Flowable<WeatherCurrentDayEntity> {
        return single(params)
    }

    class Params(
        val name: String
    )
}