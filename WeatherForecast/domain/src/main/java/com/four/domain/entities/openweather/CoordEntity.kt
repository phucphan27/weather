package com.four.domain.entities.openweather

data class CoordEntity(
    val lon: String?,
    val lat: String?
)