package com.four.data.entities

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "CitiesForSearch")
data class CitiesForSearch(
    @PrimaryKey
    @ColumnInfo(name = "Id")
    @SerializedName(value = "id") var id: Int,

    @ColumnInfo(name = "name")
    @SerializedName(value = "name") var name: String,

    @ColumnInfo(name = "country")
    @SerializedName(value = "country") var country: String,

    @Embedded
    @ColumnInfo(name = "coord")
    @SerializedName(value = "coord") var coord: Coord


)
