package com.four.data.entities

import com.google.gson.annotations.SerializedName

data class Coord(
    @SerializedName(value = "lat") var lat: String? = null,
    @SerializedName(value = "lon") var lon: String? = null
)
