package com.four.data.repository

import android.util.Log
import com.algolia.search.saas.Client
import com.algolia.search.saas.Index
import com.algolia.search.saas.Query
import com.four.data.entities.SearchResponseData
import com.four.data.mapper.SearchEntityMapper
import com.four.domain.entities.openweather.SearchResponseEntity
import com.google.gson.Gson
import io.reactivex.Flowable

class SearchApiImp constructor(private val client: Client, val gson: Gson): CityDataStore{
    private val mapper = SearchEntityMapper()
    override fun getCityWithQuery(query: String): Flowable<SearchResponseEntity> {
        return Flowable.generate { flowable ->

            val algoliaQuery = Query(query)
                .setHitsPerPage(25)

            val index: Index = client.getIndex("weather_city")
            //index.searchAsync()
            index.searchAsync(algoliaQuery) {json, exception ->
                if(exception == null){
                    try{
                        val adapter = gson.getAdapter(SearchResponseData::class.java)
                        val data = adapter.fromJson(json.toString())
                        if(data?.cities != null){
                            flowable.onNext(mapper.mapSearchResponseToEntity(data))
                            flowable.onComplete()
                        }
                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }
            }
        }


    }

}