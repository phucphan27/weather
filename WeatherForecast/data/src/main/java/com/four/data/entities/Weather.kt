package com.four.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "weather")
data class Weather(
    @PrimaryKey @SerializedName(value = "id") var id: Int,
    @SerializedName(value = "icon") var icon: String? = null,
    @SerializedName(value = "main") var main: String? = null
)