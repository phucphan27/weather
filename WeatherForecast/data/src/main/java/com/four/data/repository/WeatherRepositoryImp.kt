package com.four.data.repository

import android.annotation.SuppressLint
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.entities.openweather.WeatherNextDayEntity
import com.four.domain.repositories.WeatherRepository
import io.reactivex.Flowable

class WeatherRepositoryImp(
    private val api: WeatherApiImp,
    private val cache: WeatherCacheImp
) : WeatherRepository {

    @SuppressLint("CheckResult")
    override fun getApiWeather(name: String): Flowable<WeatherCurrentDayEntity> {
        return api.getWeather(name).doOnNext { cache.saveInfo(it) }
    }

    override fun getCacheWeather(): Flowable<MutableList<WeatherCurrentDayEntity>> {
        return cache.getWeather()
    }

    override fun getWeatherInNextDay(id: String): Flowable<WeatherNextDayEntity> {
        return api.getWeatherNextDay(id).doOnNext { cache.updateInfo(it.list, id) }
    }

    override fun getDetailWeatherInDay(id: String): Flowable<WeatherCurrentDayEntity> {
        return cache.searchCityWeather(id)
    }

    override fun removeWeatherOfCity(id: String) {
        return cache.delete(id)
    }

    override fun updateWeatherOfCity(entity: WeatherCurrentDayEntity) {
        return cache.updateWeatherOfCity(entity)
    }
}