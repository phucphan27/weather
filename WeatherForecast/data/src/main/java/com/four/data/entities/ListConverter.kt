package com.four.data.entities

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ListConverter {
    private val gson = Gson()
    @TypeConverter
    fun toList(data: String?): List<Weather> {
        if (data == null) {
            return emptyList()
        }
        val listType = object : TypeToken<List<Weather>>() {}.type
        return gson.fromJson<List<Weather>>(data, listType)
    }

    @TypeConverter
    fun toString(data: List<Weather>): String {
        return gson.toJson(data)
    }
}

class WeatherForecastConverter {
    private val gson = Gson()
    @TypeConverter
    fun toList(data: String?): List<ItemWeather> {
        if (data == null) {
            return emptyList()
        }
        val listType = object : TypeToken<List<ItemWeather>>() {}.type
        return gson.fromJson<List<ItemWeather>>(data, listType)
    }

    @TypeConverter
    fun toString(data: List<ItemWeather>): String {
        if (data.isEmpty()) return "[]"
        return gson.toJson(data)
    }
}