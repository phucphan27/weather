package com.four.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.four.data.db.openweatherDAO.WeatherDAO
import com.four.data.entities.ListConverter
import com.four.data.entities.WeatherCurrentDay
import com.four.data.entities.WeatherForecastConverter

@Database(entities = [WeatherCurrentDay::class], version = 1)
@TypeConverters(ListConverter::class, WeatherForecastConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCurrentWeatherDAO(): WeatherDAO
}