package com.four.data.entities

import com.google.gson.annotations.SerializedName

data class Wind (
    @SerializedName(value = "speed") var speed: String? = null
)