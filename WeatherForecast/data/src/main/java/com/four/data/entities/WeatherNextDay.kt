package com.four.data.entities

import com.google.gson.annotations.SerializedName

data class WeatherNextDay(
    @SerializedName(value = "cod") var cod: String,
    @SerializedName(value = "list") var list: List<ItemWeather> = emptyList()
)


data class ItemWeather(
    @SerializedName(value = "dt") var dt: Long?,
    @SerializedName(value = "dt_txt") var dtTxt: String? = null,
    @SerializedName(value = "main") var main: Main,
    @SerializedName(value = "weather") var weather: List<Weather>
)