package com.four.data.api

import com.four.data.entities.WeatherCurrentDay
import com.four.data.entities.WeatherNextDay
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WeatherApi {
    @GET("weather")
    fun getWeather(@Query("q") name: String): Flowable<WeatherCurrentDay>

    @GET("forecast/{id}")
    fun getWeatherNextDay(@Path("id") id: String?): Flowable<WeatherNextDay>

}
