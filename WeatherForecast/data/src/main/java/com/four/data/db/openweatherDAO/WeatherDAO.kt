package com.four.data.db.openweatherDAO

import androidx.room.*
import com.four.data.entities.ItemWeather
import com.four.data.entities.Weather
import com.four.data.entities.WeatherCurrentDay
import io.reactivex.Flowable

@Dao
interface WeatherDAO {
    @Query("select * from weather_current")
    fun getAll(): Flowable<MutableList<WeatherCurrentDay>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(vararg weatherCurrentDay: WeatherCurrentDay)

    @Query("Update weather_current set forecast=:list where id=:id")
    fun updateForecast(list: List<ItemWeather>, id: String)

    @Query("select * from weather_current where id=:id ")
    fun getOne(id: String): Flowable<WeatherCurrentDay>

    @Query("Delete from weather_current where id=:id")
    fun deleteOne(id: String)

    @Query("Update weather_current set weather=:weather and date=:date where id=:id")
    fun updateWeatherOfCity(weather: List<Weather>, date: String, id: String)
}