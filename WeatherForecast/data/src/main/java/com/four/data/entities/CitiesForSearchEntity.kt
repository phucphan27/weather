package com.four.data.entities

import android.os.Parcelable
import androidx.room.*
import com.four.domain.entities.openweather.CoordEntity
import com.four.domain.entities.openweather.HitsItemEntity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "CitiesForSearch")
data class CitiesForSearchEntity(
    @PrimaryKey
    @ColumnInfo(name = "Id")
    @SerializedName(value = "id") var id: String?,

    @ColumnInfo(name = "name")
    @SerializedName(value = "name") var name: String,

    @ColumnInfo(name = "country")
    @SerializedName(value = "country") var country: String?,

    @Embedded
    @ColumnInfo(name = "coord")
    @SerializedName(value = "coord") var coord: CoordEntity?


)
