package com.four.data.mapper

import com.four.data.entities.*
import com.four.domain.entities.openweather.*
import com.four.domain.entities.openweather.ItemWeather

class WeatherEntityMapper {

    fun mapToEntity(data: WeatherCurrentDay): WeatherCurrentDayEntity {
        var checkList = emptyList<ItemWeather>()
        if (!data.forecast.isNullOrEmpty()) {
            checkList = mapItemWeatherToEntity(data.forecast)
        }
        return WeatherCurrentDayEntity(
            main = mapToEntity(data.main),
            coord = mapToEntity(data.coord),
            weather = mapToEntity(data.weather),
            wind = mapToEntity(data.wind),
            name = data.name,
            date = data.date,
            id = data.id,
            forecast = checkList
        )
    }

    fun mapToEntity(data: WeatherNextDay): WeatherNextDayEntity = WeatherNextDayEntity(
        cod = data.cod,
        list = data.list.map {
            ItemWeather(
                dt = it.dt,
                dtTxt = it.dtTxt,
                main = mapToEntity(it.main),
                weather = it.weather.map { wt -> mapToWeatherEntity(wt) }
            )
        }
    )


    fun mapFromEntity(data: WeatherCurrentDayEntity): WeatherCurrentDay {
        var checkList = emptyList<com.four.data.entities.ItemWeather>()
        if (!data.forecast.isNullOrEmpty()) {
            checkList = data.forecast.map { mapFromEntity(it) }
        }
        return WeatherCurrentDay(
            id = data.id,
            main = mapFromEntity(data.main),
            weather = data.weather.map { mapFromEntity(it) },
            name = data.name,
            coord = mapFromEntity(data.coord),
            date = data.date,
            wind = mapFromEntity(data.wind),
            forecast = checkList
        )
    }

    fun mapFromEntity(data: ItemWeather): com.four.data.entities.ItemWeather =
        com.four.data.entities.ItemWeather(
            dtTxt = data.dtTxt,
            weather = data.weather.map { mapFromEntity(it) },
            main = mapFromEntity(data.main),
            dt = data.dt
        )

    private fun mapFromEntity(data: MainEntity): Main =
        Main(
            temp = data.temp,
            temp_max = data.temp_max,
            temp_min = data.temp_min,
            humidity = data.humidity,
            pressure = data.pressure
        )

    private fun mapFromEntity(data: CoordEntity): Coord =
        Coord(
            lon = data.lon,
            lat = data.lat
        )

    private fun mapFromEntity(data: WeatherEntity): Weather =
        Weather(
            id = data.id,
            icon = data.icon,
            main = data.main
        )

    private fun mapFromEntity(data: WindEntity): Wind =
        Wind(
            speed = data.speed
        )

    private fun mapToWeatherEntity(data: Weather): WeatherEntity = WeatherEntity(
        id = data.id,
        main = data.main, icon = data.icon
    )

    private fun mapToEntity(data: List<Weather>): List<WeatherEntity> =
        listOf(
            WeatherEntity(
                id = data[0].id,
                icon = data[0].icon,
                main = data[0].main
            )
        )

    private fun mapItemWeatherToEntity(data: List<com.four.data.entities.ItemWeather>): List<ItemWeather> {
        return data.map {
            ItemWeather(
                dtTxt = it.dtTxt,
                main = mapToEntity(it.main),
                weather = it.weather.map { weather -> mapToWeatherEntity(weather) },
                dt = it.dt
            )
        }
    }

    private fun mapToEntity(data: Main): MainEntity =
        MainEntity(
            temp = data.temp,
            temp_max = data.temp_max,
            temp_min = data.temp_min,
            humidity = data.humidity,
            pressure = data.pressure

        )

    private fun mapToEntity(data: Wind): WindEntity =
        WindEntity(
            speed = data.speed
        )

    private fun mapToEntity(data: Coord): CoordEntity =
        CoordEntity(
            lat = data.lat,
            lon = data.lon
        )

}
