package com.four.data.entities

import com.google.gson.annotations.SerializedName

data class SearchResponseData(
    @SerializedName(value = "hits") var cities: List<CitiesForSearch>
)