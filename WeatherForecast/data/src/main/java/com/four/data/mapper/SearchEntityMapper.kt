package com.four.data.mapper

import com.four.data.entities.CitiesForSearch
import com.four.data.entities.Coord
import com.four.data.entities.SearchResponseData
import com.four.domain.entities.openweather.CoordEntity
import com.four.domain.entities.openweather.HitsItemEntity
import com.four.domain.entities.openweather.SearchResponseEntity

class SearchEntityMapper { // map the data's entity to domain's entity
    fun mapListCityToEntity(cities: List<CitiesForSearch>): List<HitsItemEntity>
     = cities.map { mapCityToEntity(it) }

    fun mapSearchResponseToEntity(data: SearchResponseData): SearchResponseEntity
     = SearchResponseEntity(
        hits = mapListCityToEntity(data.cities)
    )

    private fun mapCityToEntity(data: CitiesForSearch): HitsItemEntity
    = HitsItemEntity(
        id = data.id,
        coord = mapCoordToEntity(data.coord),
        name = data.name,
        country = data.country
    )

    private fun mapCoordToEntity(coord: Coord) : CoordEntity
     = CoordEntity(
        lat = coord.lat,
        lon = coord.lon
    )

    fun mapListCityFromEntity(cities: List<HitsItemEntity>) : List<CitiesForSearch>
            = cities.map { mapCityFromEntity(it) }

    fun mapSearchResponseFromEntity(data: SearchResponseEntity) : SearchResponseData
            = SearchResponseData(
        cities = mapListCityFromEntity(data.hits)
    )

    fun mapCityFromEntity(data: HitsItemEntity): CitiesForSearch
            = CitiesForSearch(
        id = data.id,
        coord = mapCoordFromEntity(data.coord),
        country = data.country,
        name = data.name
    )

    private fun mapCoordFromEntity(coord: CoordEntity): Coord
            = Coord(
        lon = coord.lon,
        lat = coord.lat
    )

}
