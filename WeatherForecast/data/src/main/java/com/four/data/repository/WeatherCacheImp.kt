package com.four.data.repository

import android.annotation.SuppressLint
import com.four.data.db.AppDatabase
import com.four.data.db.openweatherDAO.*
import com.four.data.mapper.WeatherEntityMapper
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import io.reactivex.Flowable

class WeatherCacheImp(database: AppDatabase) :
    WeatherDataStore {
    private val weatherDao: WeatherDAO = database.getCurrentWeatherDAO()
    private val mapper = WeatherEntityMapper()

    @SuppressLint("CheckResult")
    override fun getWeather(): Flowable<MutableList<WeatherCurrentDayEntity>> {
        return weatherDao.getAll().map {
            it.map { wcd -> mapper.mapToEntity(wcd) }.toMutableList()
        }
    }

    fun saveInfo(weatherCurrentDayEntity: WeatherCurrentDayEntity) {
        weatherDao.saveAll(mapper.mapFromEntity(weatherCurrentDayEntity))
    }

    fun updateInfo(list: List<com.four.domain.entities.openweather.ItemWeather>, id: String) {
        weatherDao.updateForecast(list.map { mapper.mapFromEntity(it) }, id)
    }

    fun searchCityWeather(id: String): Flowable<WeatherCurrentDayEntity> {
        return weatherDao.getOne(id).map { mapper.mapToEntity(it) }
    }

    fun delete(id: String) {
        return weatherDao.deleteOne(id)
    }

    fun updateWeatherOfCity(entity: WeatherCurrentDayEntity) {
        val weatherCurrentDay = mapper.mapFromEntity(entity)
        return weatherDao.updateWeatherOfCity(
            weatherCurrentDay.weather,
            weatherCurrentDay.date,
            weatherCurrentDay.id
        )
    }
}