package com.four.data.repository

import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import io.reactivex.Flowable

interface WeatherDataStore {
    fun getWeather(): Flowable<MutableList<WeatherCurrentDayEntity>>
}