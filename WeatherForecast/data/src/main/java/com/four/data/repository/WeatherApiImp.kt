package com.four.data.repository

import com.four.data.api.WeatherApi
import com.four.data.mapper.WeatherEntityMapper
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.entities.openweather.WeatherNextDayEntity
import io.reactivex.Flowable

class WeatherApiImp constructor(private val api: WeatherApi) {

    private val mapper = WeatherEntityMapper()

    fun getWeather(name: String): Flowable<WeatherCurrentDayEntity> {
        return api.getWeather(name).map {
            mapper.mapToEntity(it)
        }
    }

    fun getWeatherNextDay(id: String?): Flowable<WeatherNextDayEntity> {
        return api.getWeatherNextDay(id).map { mapper.mapToEntity(it) }
    }
}