package com.four.data.entities

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "weather_current")
data class WeatherCurrentDay(
    @PrimaryKey
    @SerializedName(value = "id")
    var id: String,

    @Embedded(prefix = "main_")
    @SerializedName(value = "main")
    var main: Main,

    @Embedded(prefix = "coord_")
    @SerializedName(value = "coord")
    var coord: Coord,

    @SerializedName(value = "weather")
    @TypeConverters(ListConverter::class)
    var weather: List<Weather>,

    @Embedded(prefix = "wind_")
    @SerializedName(value = "wind")
    var wind: Wind,

    @TypeConverters(WeatherForecastConverter::class)
    val forecast: List<ItemWeather> = emptyList(),

    @SerializedName(value = "name") var name: String,
    @SerializedName(value = "dt") var date: String
)