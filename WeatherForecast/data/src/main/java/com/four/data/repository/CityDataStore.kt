package com.four.data.repository

import com.four.domain.entities.openweather.SearchResponseEntity
import io.reactivex.Flowable

interface CityDataStore {
    fun getCityWithQuery(query: String): Flowable<SearchResponseEntity>
}