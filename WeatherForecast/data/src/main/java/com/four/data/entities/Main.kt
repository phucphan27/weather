package com.four.data.entities

import com.google.gson.annotations.SerializedName

data class Main(
    @SerializedName(value = "temp") var temp: String? = null,
    @SerializedName(value = "temp_min") var temp_min: String? = null,
    @SerializedName(value = "temp_max") var temp_max:String?=null,
    @SerializedName(value = "humidity") var humidity:String?=null,
    @SerializedName(value = "pressure") var pressure:String?=null
)