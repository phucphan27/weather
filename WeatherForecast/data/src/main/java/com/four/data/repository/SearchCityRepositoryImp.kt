package com.four.data.repository

import com.four.domain.entities.openweather.SearchResponseEntity
import com.four.domain.repositories.SearchRepository
import io.reactivex.Flowable

class SearchCityRepositoryImp (
    private val api: SearchApiImp
): SearchRepository {
    override fun getCityWithQuery(query: String): Flowable<SearchResponseEntity> {
        return api.getCityWithQuery(query)
    }

}