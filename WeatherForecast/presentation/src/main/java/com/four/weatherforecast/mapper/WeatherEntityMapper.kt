package com.four.weatherforecast.mapper

import com.four.domain.common.Mapper
import com.four.domain.entities.openweather.MainEntity
import com.four.domain.entities.openweather.CoordEntity
import com.four.domain.entities.openweather.ItemWeather
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.entities.openweather.WeatherEntity
import com.four.domain.entities.openweather.WindEntity
import com.four.weatherforecast.entities.openweather.*

class WeatherEntityMapper : Mapper<WeatherCurrentDayEntity, WeatherCurrentDay>() {

    override fun mapFrom(data: WeatherCurrentDayEntity): WeatherCurrentDay {
        var checkList = emptyList<com.four.weatherforecast.entities.openweather.ItemWeather>()
        if (!data.forecast.isNullOrEmpty()) {
            checkList = (data.forecast.map { mapFromEntity(it) })
        }
        return WeatherCurrentDay(
            main = mapFromEntity(data.main),
            coord = mapFromEntity(data.coord),
            wind = mapFromEntity(data.wind),
            name = data.name,
            date = data.date,
            id = data.id,
            weather = data.weather.map { mapFromEntity(it) },
            forecast = checkList
        )
    }


    private fun mapFromEntity(data: WeatherEntity): Weather = Weather(
        main = data.main,
        icon = data.icon,
        id = data.id
    )

    private fun mapFromEntity(data: MainEntity): Main =
        Main(
            temp = data.temp,
            temp_max = data.temp_max,
            temp_min = data.temp_min,
            humidity = data.humidity,
            pressure = data.pressure
        )

    private fun mapFromEntity(data: CoordEntity): Coord =
        Coord(
            lon = data.lon,
            lat = data.lat
        )

    private fun mapFromEntity(data: WindEntity): Wind =
        Wind(
            id = data.id,
            speed = data.speed
        )

    private fun mapFromEntity(data: ItemWeather): com.four.weatherforecast.entities.openweather.ItemWeather =
        ItemWeather(
            dt = data.dt,
            main = mapFromEntity(data.main),
            weather = data.weather.map { mapFromEntity(it) },
            dtTxt = data.dtTxt
        )
}