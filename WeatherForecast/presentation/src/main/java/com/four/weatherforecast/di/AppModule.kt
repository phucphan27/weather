package com.four.weatherforecast.di

import androidx.room.Room
import com.four.data.api.WeatherApi
import com.four.data.db.AppDatabase
import com.four.data.repository.*
import com.four.domain.repositories.SearchRepository
import com.four.domain.repositories.WeatherRepository
import com.four.domain.usecases.*
import com.four.weatherforecast.common.AsyncFlowableTransformer
import com.four.weatherforecast.mapper.SearchEntityMapper
import com.four.weatherforecast.mapper.WeatherEntityMapper
import com.four.weatherforecast.mapper.WeatherNextDayMapper
import com.four.weatherforecast.ui.day.DetailWeatherViewModel
import com.four.weatherforecast.ui.detail.DetailViewModel
import com.four.weatherforecast.ui.main.WeatherViewModel
import com.four.weatherforecast.ui.search.SearchViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

val mNetworkModules = module {
    single(name = "Retrofit") { createNetworkInstance(BASE_URL) }
    single(name = "Api") { (get("Retrofit") as Retrofit).create(WeatherApi::class.java) }
    single(name = "Search_client") { createSearchClient() }
    single(name = "Gson") { createGson() }
}

val mLocalModules = module {
    single(name = "database") {
        Room.databaseBuilder(
            androidApplication(),
            AppDatabase::class.java,
            "app"
        ).build()
    }
}

val mRepositoryModules = module {
    single(name = "remote") {
        WeatherApiImp(
            api = get("Api")
        )
    }
    single(name = "local") {
        WeatherCacheImp(
            database = get("database")
        )
    }
    single {
        WeatherRepositoryImp(
            api = get("remote"),
            cache = get("local")
        ) as WeatherRepository
    }

    single(name = "searchApi") {
        SearchApiImp(
            client = get("Search_client"),
            gson = get("Gson")
        )
    }

    single {
        SearchCityRepositoryImp(
            api = get("searchApi")
        ) as SearchRepository
    }

}

val mUseCaseModules = module {
    factory(name = "currentWeather") {
        GetCurrentWeatherOfCitiesUseCase(
            transformer = AsyncFlowableTransformer(),
            repository = get()
        )
    }
    factory(name = "weatherNextDay") {
        GetWeatherInNextDayUseCase(transformer = AsyncFlowableTransformer(), repository = get())
    }
    factory(name = "searchCity") {
        SearchCityUseCase(transformer = AsyncFlowableTransformer(), repository = get())
    }
    factory(name = "searchWeather") {
        SearchWeatherOfCityUseCase(transformer = AsyncFlowableTransformer(), repository = get())
    }
    factory(name = "getMoreDetail") {
        GetDetailWeatherInDayUseCase(transformer = AsyncFlowableTransformer(), repository = get())
    }
    factory(name = "removeCity") {
        RemoveWeatherUseCase(repository = get())
    }
    factory(name = "updateWeather") {
        UpdateWeatherOfCity(repository = get())
    }
}
val mViewModels = module {
    viewModel(name = "apiVM") {
        WeatherViewModel(
            getCurrentWeatherOfCitiesUseCase = get("currentWeather"),
            searchWeatherOfCityUseCase = get("searchWeather"),
            removeWeatherUseCase = get("removeCity"),
            updateWeatherUseCase = get("updateWeather"),
            mapper = WeatherEntityMapper()
        )
    }
    viewModel(name = "apiNextDay") {
        DetailViewModel(
            getWeatherInNextDayUseCase = get("weatherNextDay"),
            mapper = WeatherNextDayMapper()
        )
    }
    viewModel(name = "searchVM") {
        SearchViewModel(
            searchCityUseCase = get("searchCity"),
            mapper = SearchEntityMapper()
        )
    }
    viewModel(name = "detailDay") {
        DetailWeatherViewModel(
            mapper = WeatherEntityMapper(),
            getDetailWeatherInDayUseCase = get("getMoreDetail")
        )
    }
}
private const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
