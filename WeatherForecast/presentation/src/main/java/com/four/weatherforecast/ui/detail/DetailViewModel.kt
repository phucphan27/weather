package com.four.weatherforecast.ui.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.four.domain.common.Mapper
import com.four.domain.entities.openweather.WeatherNextDayEntity
import com.four.domain.usecases.GetWeatherInNextDayUseCase
import com.four.weatherforecast.common.BaseViewModel
import com.four.weatherforecast.entities.openweather.ItemWeather
import com.four.weatherforecast.entities.openweather.WeatherNextDay
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DetailViewModel(
    private val getWeatherInNextDayUseCase: GetWeatherInNextDayUseCase,
    private val mapper: Mapper<WeatherNextDayEntity, WeatherNextDay>
) : BaseViewModel() {
    private var mWeather = MutableLiveData<WeatherNextDay>()
    var cityLiveData = mWeather
    private var weatherCode = MutableLiveData("")
    val weatherCodeLiveData: LiveData<String> = weatherCode
    fun fetchData(id: String, code: String, forecast: String, isAvailableNetwork: Boolean) {
        weatherCode.value = code
        if (isAvailableNetwork) {
            val disposable = getWeatherInNextDayUseCase.getWeatherNextDay(GetWeatherInNextDayUseCase.Params(id))
                .flatMap { mapper.Flowable(it) }
                .subscribe({ response ->
                    Log.d("viewModel", "On next called")
                    mWeather.value = response
                }, { error ->
                    Log.d("viewModel error", error.message)
                }, {
                    Log.d("viewModel", "On Complete")
                })
            addDisposable(disposable)
        } else if(forecast != "") {
            val gSon = Gson()
            val listType = object : TypeToken<List<ItemWeather>>() {}.type
            var wnd = WeatherNextDay()
            wnd.list = gSon.fromJson<List<ItemWeather>>(forecast, listType)
            mWeather.value = wnd
        }
    }

    fun getWeatherLiveData() = cityLiveData
}