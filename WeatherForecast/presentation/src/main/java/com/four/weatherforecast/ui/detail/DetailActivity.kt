package com.four.weatherforecast.ui.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.four.weatherforecast.R
import com.four.weatherforecast.databinding.ActivityDetailBinding
import com.four.weatherforecast.utils.extensions.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.viewmodel.ext.android.viewModel


class DetailActivity : AppCompatActivity() {
    private val listAdapter: WeatherListAdapter = WeatherListAdapter()
    private val detailVM: DetailViewModel by viewModel(name = "apiNextDay")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingAdapter(detailVM)
        detail_recycle_view.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        detail_recycle_view.adapter = listAdapter
        val id: String? = intent.getStringExtra("id")
        val weatherCode: Int? = intent.getIntExtra("weatherCode", 0)
        val forecast: String = intent.getStringExtra("forecast")
        detailVM.fetchData(id!!, weatherCode.toString(), forecast, isNetworkAvailable(this))

        setAnimation()
        changeStatusBar()
        bindingCurrentInfo()
    }

    override fun onStart() {
        super.onStart()
        detailVM.getWeatherLiveData().observe(this, Observer {
            listAdapter.updateList(
                it.list, intent.getStringExtra("id")!!
            )
        })
    }

    private fun changeStatusBar() {
        val window: Window = this.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.statusBar)
    }

    private fun setAnimation() {
        val upToDown = AnimationUtils.loadAnimation(this, R.anim.uptodown)
        val downToUp = AnimationUtils.loadAnimation(this, R.anim.downtoup)
        val leftToRight = AnimationUtils.loadAnimation(this, R.anim.lefttoright)
        val rightToLeft = AnimationUtils.loadAnimation(this, R.anim.righttoleft)
        txtCityTitle.animation = upToDown
        txtWeather.animation = downToUp
        txtTemp.animation = rightToLeft
        txtWind.animation = leftToRight
        txtRain.animation = leftToRight
        txtHum.animation = leftToRight
        txtWindV.animation = leftToRight
        txtRainV.animation = leftToRight
        txtHumV.animation = leftToRight
        txtUnitH.animation = leftToRight
        txtUnitR.animation = leftToRight
        txtUnitW.animation = leftToRight
    }

    @SuppressLint("SetTextI18n")
    private fun bindingCurrentInfo() {
        val name: String? = intent.getStringExtra("name")
        val main: String? = intent.getStringExtra("main")
        val temp: String? = intent.getStringExtra("temp")
        val humidity: String? = intent.getStringExtra("humidity")
        val speed: String? = intent.getStringExtra("speed")
        val pressure: String? = intent.getStringExtra("pressure")
        temp?.let {
            txtTemp.text = "${(it.toFloat() - 273.15f).toInt()}℃"
        }
        txtCityTitle.text = name
        txtWeather.text = main
        txtHumV.text = humidity
        txtWindV.text = speed
        txtRainV.text = pressure
    }

    private fun bindingAdapter(detailVM: DetailViewModel) {
        val binding: ActivityDetailBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.lifecycleOwner = this
        binding.viewmodel = detailVM
    }
}
