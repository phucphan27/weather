package com.four.weatherforecast.ui.search

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.four.weatherforecast.R
import com.four.weatherforecast.entities.openweather.HitsItem
import kotlinx.android.synthetic.main.item_search_result.view.*

class SearchResultAdapter(private var activity: Activity) :
    RecyclerView.Adapter<SearchResultAdapter.SearchResultViewHolder>() {
    private var mHitsItemList = mutableListOf<HitsItem>()

    class SearchResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private lateinit var hitsItem: HitsItem
        private lateinit var activity: Activity

        fun bind(hitsItem: HitsItem, activity: Activity) {
            this.hitsItem = hitsItem
            this.activity = activity
            itemView.textViewCityName.text = hitsItem.name.plus(", ").plus(hitsItem.country)
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            var intent = Intent()
            intent.putExtra("city_name", hitsItem.name)
            activity.setResult(RESULT_OK, intent)
            activity.finish()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        return SearchResultViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_search_result,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return mHitsItemList.size
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        holder.bind(mHitsItemList[position], activity)
    }

    fun update(hitsItem: MutableList<HitsItem>) {
        if (hitsItem.isNotEmpty()) {
            mHitsItemList.clear()
            mHitsItemList.addAll(hitsItem)
            notifyDataSetChanged()
        }
    }
}