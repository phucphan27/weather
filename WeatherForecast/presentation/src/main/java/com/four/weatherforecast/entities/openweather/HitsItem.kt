package com.four.weatherforecast.entities.openweather

class HitsItem (
    val id: Int,
    val name: String,
    val country: String,
    val coord: Coord
)