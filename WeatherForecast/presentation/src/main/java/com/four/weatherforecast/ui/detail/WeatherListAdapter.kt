package com.four.weatherforecast.ui.detail

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.four.weatherforecast.R
import com.four.weatherforecast.entities.openweather.ItemWeather
import com.four.weatherforecast.ui.day.DetailWeatherActivity
import kotlinx.android.synthetic.main.weather_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class WeatherListAdapter : RecyclerView.Adapter<WeatherListAdapter.ViewHolder>() {

    private var itemWeather = mutableListOf<ItemWeather>()
    private var idCity = MutableLiveData<String>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val intent = Intent(itemView.context, DetailWeatherActivity::class.java)

        init {
            itemView.setOnClickListener { itemView.context.startActivity(intent) }
        }

        fun bind(item: ItemWeather, id: String?) {
            intent.putExtra("idCity", id)
            intent.putExtra("datePicker", item.dt)
            with(itemView) {
                val temp = convertTemperature(item.main.temp)
                Glide.with(this)
                    .load("http://openweathermap.org/img/wn/" + item.weather[0].icon + ".png")
                    .into(iconWeather)
                txtDayOfWeek.text = convertDate(item.dt)
                txtTempC.text = temp
            }
        }

        private fun convertDate(dateBefore: Long?): String {
            val date = Date(dateBefore!! * 1000L)
            val simpleDateFormat = SimpleDateFormat("EE, dd-MMM")
            return simpleDateFormat.format(date)
        }

        private fun convertTemperature(temp: String?): String {
            return (temp!!.toFloat() - 273.15f).toInt().toString() + "℃"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.weather_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return itemWeather.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemWeather[position], idCity.value)
    }

    fun updateList(list: List<ItemWeather>, id: String) {
        if (list.isNotEmpty()) {
            idCity.value = id
            itemWeather.clear()
            list.map {
                if (it.dtTxt!!.contains("12:00:00")) {
                    itemWeather.add(it)
                }
            }
            notifyDataSetChanged()
        }
    }


}
