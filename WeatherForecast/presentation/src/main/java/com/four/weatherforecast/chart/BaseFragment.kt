package com.four.weatherforecast.chart

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.four.weatherforecast.R
import com.four.weatherforecast.common.BaseViewModel
import java.util.Observer

abstract class BaseFragment : Fragment() {
    protected val TAG = BaseFragment::class.java.simpleName

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupUI(view)
    }



    abstract fun setupViewModel()

    abstract fun setupUI(view: View)
}