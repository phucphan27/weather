package com.four.weatherforecast.mapper

import com.four.domain.common.Mapper
import com.four.domain.entities.openweather.CoordEntity
import com.four.domain.entities.openweather.HitsItemEntity
import com.four.domain.entities.openweather.SearchResponseEntity
import com.four.weatherforecast.entities.openweather.Coord
import com.four.weatherforecast.entities.openweather.HitsItem
import com.four.weatherforecast.entities.openweather.SearchResponse

class SearchEntityMapper : Mapper<SearchResponseEntity, SearchResponse>(){
    override fun mapFrom(data: SearchResponseEntity): SearchResponse =
        SearchResponse(
            hits = mapListHitsFromEntity(data.hits)
        )

    private fun mapListHitsFromEntity(data: List<HitsItemEntity>) : List<HitsItem>
        = data.map {
        mapHitsFromEntity(it)
    }

    private fun mapHitsFromEntity(data: HitsItemEntity) : HitsItem
        = HitsItem(
        id = data.id,
        name = data.name,
        country = data.country,
        coord = mapCoordFromEntity(data.coord)
    )

    private fun mapCoordFromEntity(coord: CoordEntity): Coord
    = Coord(
        lat = coord.lat,
        lon = coord.lon
    )
}