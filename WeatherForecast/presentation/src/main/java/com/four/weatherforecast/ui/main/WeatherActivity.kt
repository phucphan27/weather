package com.four.weatherforecast.ui.main

import android.app.Activity
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.four.weatherforecast.R
import com.four.weatherforecast.databinding.ActivityCityBinding
import com.four.weatherforecast.ui.search.SearchActivity
import com.four.weatherforecast.utils.extensions.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_city.*
import org.koin.android.viewmodel.ext.android.viewModel


class WeatherActivity : AppCompatActivity() {

    private val weather: WeatherViewModel by viewModel(name = "apiVM")
    private lateinit var listAdapter: CityListAdapter
    private lateinit var searchItem: MenuItem
    private val REQUEST_CITY_SEARCH = 1
    private val REQUEST_CITY_ADD = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city)
        val binding: ActivityCityBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_city)
        binding.lifecycleOwner = this
        binding.viewmodel = weather

        listAdapter = CityListAdapter()
        recycler_view.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_view.adapter = listAdapter

        weather.fetchData()
        networkHandler()
        initFab()

//        floatingActionButton.setOnClickListener {
//            weather.callApi()
//            weather.fetchData(isNetworkAvailable(this))
//        }
    }


    override fun onStart() {
        super.onStart()
        weather.getWeatherLiveData().observe(this, Observer {
            it?.let { res ->
                listAdapter.updateList(res, object : Callback {
                    override fun removeWeatherOfCity(position: Int) {
                        weather.removeCity(listAdapter.getCityId(position))
                        listAdapter.removeAt(position)
                    }
                })
            }
        })
        if (isNetworkAvailable(this)) {
//            weather.updateDataFromApi()
        }
    }

    interface Callback {
        fun removeWeatherOfCity(position: Int)
    }

    private fun networkHandler() {
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.registerDefaultNetworkCallback(object :
            ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                weather.fetchData()
            }

            override fun onLost(network: Network?) {
            }
        })
    }

    private fun initSearchView(menu: Menu?) {
        val inflater = menuInflater
        inflater.inflate(R.menu.search_menu, menu)
        searchItem = menu!!.findItem(R.id.action_search)
    }

    private fun initFab() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        fabAddCity.setOnClickListener {
            var intent = Intent(this, SearchActivity::class.java)
            startActivityForResult(intent, REQUEST_CITY_ADD)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                var intent = Intent(this, SearchActivity::class.java)
                startActivityForResult(intent, REQUEST_CITY_SEARCH)
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            // Click Search Menu
            REQUEST_CITY_SEARCH -> {
                if (resultCode == Activity.RESULT_OK) {
                    // get city name -> navigate to detail activity with city name
                    Toast.makeText(
                        this,
                        "Search" + data?.getStringExtra("city_name"),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            // Click add fab
            REQUEST_CITY_ADD -> {
                if (resultCode == Activity.RESULT_OK) {
                    weather.run { callApi(data!!.getStringExtra("city_name")) }
                    // get city name -> add to weather city list -> reload data
                    Toast.makeText(
                        this,
                        "ADD" + data?.getStringExtra("city_name"),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}
