package com.four.weatherforecast.ui.day

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.four.domain.common.Mapper
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.usecases.GetDetailWeatherInDayUseCase
import com.four.weatherforecast.common.BaseViewModel
import com.four.weatherforecast.entities.openweather.ItemWeather
import com.four.weatherforecast.entities.openweather.WeatherCurrentDay
import java.text.SimpleDateFormat
import java.util.*

class DetailWeatherViewModel(
    private val mapper: Mapper<WeatherCurrentDayEntity, WeatherCurrentDay>,
    private val getDetailWeatherInDayUseCase: GetDetailWeatherInDayUseCase
) : BaseViewModel() {
    private var mWeather = MutableLiveData<List<ItemWeather>>()
    var cityLiveData: LiveData<List<ItemWeather>> = mWeather
    fun fetchData(idCity: String, datePicker: Long) {
        var disposable =
            getDetailWeatherInDayUseCase.getDetailDay(GetDetailWeatherInDayUseCase.Params(idCity))
                .flatMap { mapper.Flowable(it) }
                .subscribe({ res ->
                    mWeather.value = processData(datePicker, res)
                    Log.d("vm response", "OK")
                }, { error ->
                    Log.d("vm error", error.message)
                }, {
                    Log.d("vm complete", "complete")
                })
        addDisposable(disposable)
    }

    fun getWeatherLiveData() = cityLiveData

    /**
     * @return
     *  filter usable data for binding
     * */
    private fun processData(datePicker: Long, wcr: WeatherCurrentDay): List<ItemWeather> {
        val listForecast = wcr.forecast
        return listForecast.filter { compareDate(datePicker, it.dtTxt) }
    }

    @Suppress("DEPRECATION")
    /**
     * @return
     *  true: date in list is same with date picker
     * */
    private fun compareDate(datePicker: Long, dateInList: String?): Boolean {
        if (!dateInList!!.contains(convertDate(datePicker))) return false
        return true
    }

    /**
     * @return
     *  convert from Long to String
     * */
    private fun convertDate(dateBefore: Long?): String {
        val date = Date(dateBefore!! * 1000L)
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        return simpleDateFormat.format(date)
    }
}