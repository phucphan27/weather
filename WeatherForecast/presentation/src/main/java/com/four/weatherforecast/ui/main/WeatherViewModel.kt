package com.four.weatherforecast.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.four.domain.common.Mapper
import com.four.domain.entities.openweather.WeatherCurrentDayEntity
import com.four.domain.usecases.GetCurrentWeatherOfCitiesUseCase
import com.four.domain.usecases.RemoveWeatherUseCase
import com.four.domain.usecases.SearchWeatherOfCityUseCase
import com.four.domain.usecases.UpdateWeatherOfCity
import com.four.weatherforecast.common.BaseViewModel
import com.four.weatherforecast.entities.openweather.WeatherCurrentDay
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class WeatherViewModel(
    private val getCurrentWeatherOfCitiesUseCase: GetCurrentWeatherOfCitiesUseCase,
    private val searchWeatherOfCityUseCase: SearchWeatherOfCityUseCase,
    private val removeWeatherUseCase: RemoveWeatherUseCase,
    private val updateWeatherUseCase: UpdateWeatherOfCity,
    private val mapper: Mapper<WeatherCurrentDayEntity, WeatherCurrentDay>
) : BaseViewModel() {
    private var mWeather = MutableLiveData<List<WeatherCurrentDay>>()

    fun fetchData() {
        val disposable =
            getCurrentWeatherOfCitiesUseCase.getWeather()
                .flatMap { mapper.Flowable(it) }
                .subscribe({ response ->
                    Log.d("viewModel", "On next called")
                    mWeather.value = response

                }, { error ->
                    Log.d("viewModel error", error.message)
                }, {
                    Log.d("viewModel", "On Complete")
                })
        addDisposable(disposable)
    }

    fun updateDataFromApi() {
        mWeather.value?.map { wcd ->
            val disposable =
                searchWeatherOfCityUseCase.getWeather(SearchWeatherOfCityUseCase.Params(wcd.name))
                    .subscribe({ res ->
                        addDisposable(Observable.just(1)
                            .subscribeOn(Schedulers.newThread())
                            .subscribe { updateWeatherUseCase.updateWeatherOfCity(res) }
                        )
                    }, { error -> Log.d("viewModel error", error.message) })
            addDisposable(disposable)
        }
        fetchData()
    }

    fun callApi(name: String) {
        val disposable =
            searchWeatherOfCityUseCase.getWeather(SearchWeatherOfCityUseCase.Params(name))
                .flatMap { mapper.Flowable(it) }
                .subscribe({ response ->
                    mWeather.value?.toMutableList()?.add(response)
                    Log.d("viewModel", "On next called")
                }, { error ->
                    Log.d("viewModel error", error.message)
                }, {
                    Log.d("viewModel", "On Complete")
                })
        addDisposable(disposable)
    }

    fun removeCity(id: String) {
        addDisposable(Observable.just(id)
            .subscribeOn(Schedulers.io())
            .subscribe { removeWeatherUseCase.removeCity(id) }
        )
    }

    fun getWeatherLiveData() = mWeather

}