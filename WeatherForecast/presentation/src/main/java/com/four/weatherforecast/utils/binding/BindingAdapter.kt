package com.four.weatherforecast.utils.binding

import android.graphics.Color
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import com.four.weatherforecast.R
import com.four.weatherforecast.chart.ChartEntity
import com.four.weatherforecast.chart.LineChart
import com.four.weatherforecast.entities.openweather.ItemWeather
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@BindingAdapter("bg")
fun setBg(view: ConstraintLayout, code: String) {
    when {
        code.startsWith(Constants.THUNDER_STORM) -> view.setBackgroundResource(R.drawable.thunderstorm)
        code.startsWith(Constants.DRIZZLE) -> view.setBackgroundResource(R.drawable.rainny2)
        code.startsWith(Constants.RAIN) -> view.setBackgroundResource(R.drawable.rainny2)
        code.startsWith(Constants.SNOW) -> view.setBackgroundResource(R.drawable.snow)
        code.startsWith(Constants.ATMOSPHERE) -> view.setBackgroundResource(R.drawable.atmos)
        code.startsWith(Constants.CLOUD) -> view.setBackgroundResource(R.drawable.cloud)
        else -> view.setBackgroundResource(R.drawable.mainwall)
    }
}


@BindingAdapter("wl")
fun setWl(view: LineChart, list: LiveData<List<ItemWeather>>) {
    val temp = arrayListOf<Float>()
    val legendArr = arrayListOf<String>()
    if (list.value.isNullOrEmpty()) return
    list.value!!.map {
        temp.add(convertTemperature(it.main.temp))
        legendArr.add(convertToTimeString(it.dt))
    }

    val arrGraph = ArrayList<ChartEntity>()
    arrGraph.add(ChartEntity(Color.WHITE, convertToFloatArray(temp)))
    view.legendArray = legendArr
    view.setList(arrGraph)
}

private fun convertToFloatArray(list: ArrayList<Float>): FloatArray {
    val floatArray = FloatArray(list.size)
    list.forEachIndexed { index, value ->
        floatArray[index] = value
    }
    return floatArray
}

private fun convertTemperature(temp: String?): Float {
    return (temp!!.toFloat() - 273.15f)
}

private fun convertToTimeString(time: Long?): String {
    val date = Date(time!! * 1000L)
    val simpleDateFormat = SimpleDateFormat("HH:mm")
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"))
    return simpleDateFormat.format(date)
}

object Constants {
    const val THUNDER_STORM = "2"
    const val DRIZZLE = "3"
    const val RAIN = "5"
    const val SNOW = "6"
    const val ATMOSPHERE = "7"
    const val CLOUD = "8"
}
