package com.four.weatherforecast.ui.search

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.four.weatherforecast.databinding.ActivitySearchBinding
import com.four.weatherforecast.R
import org.koin.android.viewmodel.ext.android.viewModel

class SearchActivity : AppCompatActivity() {
    private val searchViewModel: SearchViewModel by viewModel(name = "searchVM")
    private lateinit var mBinding: ActivitySearchBinding
    private lateinit var searchResultAdapter: SearchResultAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        initSearchResultAdapter()
        initSearchView()
    }

    override fun onStop() {
        super.onStop()
        setResult(Activity.RESULT_CANCELED,intent)
    }
    fun initViewModel(){
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_search)
        mBinding.lifecycleOwner = this
        mBinding.viewModel = searchViewModel
    }

    private fun initSearchResultAdapter() {
        searchResultAdapter =
            SearchResultAdapter(this)

        mBinding.recyclerViewSearchResults.adapter = searchResultAdapter
        mBinding.recyclerViewSearchResults.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    }

    private fun initSearchView(){
        val searchEditText: EditText = mBinding.searchView.findViewById(R.id.search_src_text)
        applicationContext?.let { ContextCompat.getColor(it, R.color.mainTextColor) }
            ?.let { searchEditText.setTextColor(it) }
        applicationContext?.let { ContextCompat.getColor(it, android.R.color.darker_gray) }
            ?.let { searchEditText.setHintTextColor(it) }
        mBinding.searchView.isActivated = true
        mBinding.searchView.setIconifiedByDefault(false)
        mBinding.searchView.isIconified = false

        val searchViewSearchIcon = mBinding.searchView.findViewById<ImageView>(R.id.search_mag_icon)
        searchViewSearchIcon.setImageResource(R.drawable.ic_search)

        mBinding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                if(query!!.isNotEmpty() && query.count() > 2){
                    searchViewModel.searchWithText(query)
                    mBinding.viewModel!!.getCityLiveData().observe(this@SearchActivity, Observer {
                        it?.let { list ->
                            searchResultAdapter.update(list.hits.toMutableList())
                        }
                    })
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText!!.isNotEmpty() && newText.count() > 2){
                    searchViewModel.searchWithText(newText)
                    mBinding.viewModel!!.getCityLiveData().observe(this@SearchActivity, Observer {
                        it?.let { list ->
                            searchResultAdapter.update(list.hits.toMutableList())
                        }
                    })
                }
                return true
            }

        })
    }


}
