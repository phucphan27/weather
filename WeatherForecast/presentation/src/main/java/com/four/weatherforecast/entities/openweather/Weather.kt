package com.four.weatherforecast.entities.openweather

data class Weather(
    val id: Int,
    var icon: String? = null,
    var main: String? = null
)