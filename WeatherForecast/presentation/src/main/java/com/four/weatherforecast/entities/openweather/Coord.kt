package com.four.weatherforecast.entities.openweather

data class Coord(
    var lat: String? = null,
    var lon: String? = null
)