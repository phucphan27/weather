package com.four.weatherforecast.entities.openweather

data class Main(
    var temp: String? = null,
    var temp_max: String? = null,
    var temp_min:String? = null,
    var humidity:String? = null,
    var pressure:String?=null

)