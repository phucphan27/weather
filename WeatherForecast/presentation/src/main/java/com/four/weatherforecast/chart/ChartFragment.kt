package com.four.weatherforecast.chart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.four.weatherforecast.R
import com.four.weatherforecast.databinding.FragmentChartBinding
import com.four.weatherforecast.ui.day.DetailWeatherViewModel
import org.koin.android.viewmodel.ext.android.sharedViewModel

class ChartFragment : Fragment() {

    private val viewModel by sharedViewModel<DetailWeatherViewModel>()

    companion object {
        fun newInstance() = ChartFragment()
    }


    private var graph1 = floatArrayOf(100f, 42f, 70f, 0f, 30f, 80f)
    private var legendArr = arrayOf("12", "15", "18", "21", "0", "03")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        viewModel = getSharedViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentChartBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_chart, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewmodel = viewModel
        return binding.root
    }
}
