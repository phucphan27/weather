package com.four.weatherforecast.entities.openweather

data class SearchResponse(
    val hits: List<HitsItem>
)