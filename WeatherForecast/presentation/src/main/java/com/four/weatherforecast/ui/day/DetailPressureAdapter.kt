package com.four.weatherforecast.ui.day

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.four.weatherforecast.R
import com.four.weatherforecast.entities.openweather.ItemWeather
import kotlinx.android.synthetic.main.humidity_item.view.*
import kotlinx.android.synthetic.main.pressure_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class DetailPressureAdapter : RecyclerView.Adapter<DetailPressureAdapter.ViewHolder>() {
    private var itemWeather = mutableListOf<ItemWeather>()
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ItemWeather) {
            with(itemView) {
                val temp = (item.main.temp!!.toFloat() - 273.15f).toInt().toString() + "℃"

              //  txtTimePressDetail.text= item.detail.id//)= convertDate(item.dt)
                txtTempPressDetail.text = temp
            }
        }

        private fun convertDate(dateBefore: Long?): String {
            val date = Date(dateBefore!! * 1000L)
            val simpleDateFormat = SimpleDateFormat("DD-MMM")
            return simpleDateFormat.format(date)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.pressure_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return itemWeather.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemWeather[position])
    }
    fun updateList(list: List<ItemWeather>) {
        if (list.isNotEmpty()) {
            itemWeather.clear()
            list.map {
                if (it.dtTxt!!.contains("12:00:00")) {
                    itemWeather.add(it)
                }
            }
            notifyDataSetChanged()
        }
    }

}