package com.four.weatherforecast.entities.openweather

data class WeatherCurrentDay(
    var main: Main,
    var coord: Coord,
    var weather: List<Weather>,
    var forecast: List<ItemWeather>,
    var wind: Wind,
    var name: String,
    var date: String,
    var id: String
)