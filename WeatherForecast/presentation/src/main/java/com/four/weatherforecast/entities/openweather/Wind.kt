package com.four.weatherforecast.entities.openweather

data class Wind (
    var id: Int,
    var speed: String? = null
)