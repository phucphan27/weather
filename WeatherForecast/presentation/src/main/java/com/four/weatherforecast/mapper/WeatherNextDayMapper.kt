package com.four.weatherforecast.mapper

import com.four.domain.common.Mapper
import com.four.domain.entities.openweather.MainEntity
import com.four.domain.entities.openweather.WeatherEntity
import com.four.domain.entities.openweather.WeatherNextDayEntity
import com.four.weatherforecast.entities.openweather.ItemWeather
import com.four.weatherforecast.entities.openweather.Main
import com.four.weatherforecast.entities.openweather.Weather
import com.four.weatherforecast.entities.openweather.WeatherNextDay

class WeatherNextDayMapper : Mapper<WeatherNextDayEntity, WeatherNextDay>() {
    override fun mapFrom(data: WeatherNextDayEntity): WeatherNextDay =
        WeatherNextDay(
            cod = data.cod,
            list = data.list.map {
                ItemWeather(
                    dt = it.dt,
                    dtTxt = it.dtTxt,
                    weather = it.weather.map { wt -> mapFromEntity(wt) },
                    main = mapFromEntity(it.main)
                )
            }
        )

    private fun mapFromEntity(entity: WeatherEntity): Weather = Weather(
        id = entity.id,
        icon = entity.icon,
        main = entity.main
    )

    private fun mapFromEntity(entity: MainEntity): Main = Main(
        humidity = entity.humidity,
        temp = entity.temp,
        temp_max = entity.temp_max,
        temp_min = entity.temp_min
    )
}