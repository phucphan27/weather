package com.four.weatherforecast.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.four.weatherforecast.R
import com.four.weatherforecast.entities.openweather.WeatherCurrentDay
import com.four.weatherforecast.ui.detail.DetailActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.city_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class CityListAdapter :
    RecyclerView.Adapter<CityListAdapter.CityViewHolder>() {

    var list = mutableListOf<WeatherCurrentDay>()
    lateinit var callback: WeatherActivity.Callback

    class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val intent = Intent(itemView.context, DetailActivity::class.java)
        private lateinit var callback: WeatherActivity.Callback

        init {
            itemView.setOnClickListener {
                itemView.context.startActivity(intent)
            }
            itemView.setOnLongClickListener {
                callback.removeWeatherOfCity(adapterPosition)
                return@setOnLongClickListener true
            }
        }

        @SuppressLint("SimpleDateFormat")
        private fun convertDate(dateBefore: Long?, type: String): String {
            val date = Date(dateBefore!! * 1000L)
            val simpleDateFormat =
                if (type == "DATE")
                    SimpleDateFormat("dd-MM-YYYY")
                else SimpleDateFormat("HH:mm")
            return simpleDateFormat.format(date)
        }

        private fun passValue(wcd: WeatherCurrentDay) {
            intent.putExtra("name", wcd.name)
            intent.putExtra("main", wcd.weather[0].main)
            intent.putExtra("weatherCode", wcd.weather[0].id)
            intent.putExtra("temp", wcd.main.temp)
            intent.putExtra("humidity", wcd.main.humidity)
            intent.putExtra("speed", wcd.wind.speed)
            intent.putExtra("pressure", wcd.main.pressure)
            intent.putExtra("id", wcd.id)
            if (wcd.forecast.isNullOrEmpty()) {
                intent.putExtra("forecast", "")
            } else intent.putExtra("forecast", Gson().toJson(wcd.forecast))
        }

        @SuppressLint("SetTextI18n", "SimpleDateFormat")
        fun bind(wcd: WeatherCurrentDay, callback: WeatherActivity.Callback) {
            this.callback = callback
            passValue(wcd)

            with(itemView) {
                val temp: String = (wcd.main.temp!!.toFloat() - 273.15f).toInt().toString() + "℃"
                val dateLong = wcd.date.toLong()
                txtName.text = wcd.name
                txtTemp.text = temp
                txtRain.text = wcd.main.humidity + "%"
                txtDetail.text = wcd.weather[0].main
                txtTime.text = convertDate(dateLong, "TIME")
                txtDate.text = convertDate(dateLong, "DATE")
                Glide.with(this)
                    .load("http://openweathermap.org/img/wn/" + wcd.weather[0].icon + ".png")
                    .into(txtIcon)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.city_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.bind(
            list[position], callback
        )
    }

    fun updateList(list: List<WeatherCurrentDay>, callback: WeatherActivity.Callback) {
        if (list.isNotEmpty()) {
            this.list.clear()
            this.list.addAll(list)
            this.callback = callback
            notifyDataSetChanged()
        }
    }

    fun removeAt(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, list.size)
    }

    fun getCityId(position: Int): String {
        return list[position].id
    }

}