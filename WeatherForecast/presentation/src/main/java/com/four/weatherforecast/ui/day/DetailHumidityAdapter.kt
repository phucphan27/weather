package com.four.weatherforecast.ui.day

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.four.weatherforecast.R
import com.four.weatherforecast.entities.openweather.ItemWeather
import kotlinx.android.synthetic.main.humidity_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class DetailHumidityAdapter : RecyclerView.Adapter<DetailHumidityAdapter.ViewHolder>() {
    private var itemWeather = mutableListOf<ItemWeather>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ItemWeather) {
            with(itemView) {

                txtTimeDetail.text = convertDate(item.dt)

                txtHumDetail.text = item.main.humidity + "%"
            }
        }

        private fun convertDate(dateBefore: Long?): String {
            val date = Date(dateBefore!! * 1000L)
            val simpleDateFormat = SimpleDateFormat("HH-mm")
            return simpleDateFormat.format(date)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.humidity_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return itemWeather.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemWeather[position])
    }
    fun updateList(list: List<ItemWeather>) {
        if (list.isNotEmpty()) {
            itemWeather.clear()
            itemWeather.addAll(list)
            notifyDataSetChanged()
        }
    }


}