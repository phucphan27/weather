package com.four.weatherforecast.ui.day

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.four.weatherforecast.R
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.four.weatherforecast.databinding.ActivityDetailDayBinding
import kotlinx.android.synthetic.main.activity_detail_day.*
import org.koin.android.viewmodel.ext.android.viewModel

class DetailWeatherActivity : AppCompatActivity() {

    private lateinit var listAdapterHum: DetailHumidityAdapter
    private lateinit var listAdapterPress: DetailPressureAdapter

    private val detailVM: DetailWeatherViewModel by viewModel(name = "detailDay")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityDetailDayBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_detail_day)
        binding.lifecycleOwner = this
        binding.viewmodel = detailVM

        detailVM.fetchData(intent.getStringExtra("idCity")!!,intent.getLongExtra("datePicker",0))

        listAdapterHum = DetailHumidityAdapter()
        detail_humidity_recycler.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        detail_humidity_recycler.adapter = listAdapterHum

        changeStatusBar()
    }

    override fun onStart() {
        super.onStart()
        detailVM.getWeatherLiveData().observe(this, Observer {
            listAdapterHum.updateList(it)
        })
    }

    private fun changeStatusBar() {
        val window: Window = this.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.statusBar)
    }
}
