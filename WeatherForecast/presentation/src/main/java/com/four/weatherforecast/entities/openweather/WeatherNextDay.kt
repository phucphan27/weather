package com.four.weatherforecast.entities.openweather

class WeatherNextDay(
    var cod: String = "",
    var list: List<ItemWeather> = emptyList()
)

data class ItemWeather(
    var dt: Long?,
    var dtTxt: String? = null,
    var main: Main,
    var weather: List<Weather>
)
