package com.four.weatherforecast.ui.search

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.four.domain.common.Mapper
import com.four.domain.entities.openweather.SearchResponseEntity
import com.four.domain.usecases.SearchCityUseCase
import com.four.weatherforecast.common.BaseViewModel
import com.four.weatherforecast.entities.openweather.SearchResponse

class SearchViewModel(
    private val searchCityUseCase: SearchCityUseCase,

    private val mapper: Mapper<SearchResponseEntity, SearchResponse>
) : BaseViewModel() {
    private var mCity = MutableLiveData<SearchResponse>()

    fun searchWithText(text: String) {
        Log.d("Query", text)
        val disposable = searchCityUseCase
            .getCityWithQuery(SearchCityUseCase.SearchCityParams(text))
            .flatMap { mapper.Flowable(it) }
            .subscribe({ response ->
                Log.d("viewModel response", response.toString())
                Log.d("viewModel Search", "On next called")
                mCity.value = response
            }, { error ->
                Log.d("viewModel Search error", error.message)
            }, {
                Log.d("viewModel Search", "On Complete")
            })
        addDisposable(disposable)
        Log.d("Query", "123123")
    }

    fun getCityLiveData() = mCity
}